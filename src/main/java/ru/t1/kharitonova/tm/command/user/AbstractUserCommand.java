package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
